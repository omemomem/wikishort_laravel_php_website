<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<style>

    input {
        border: none;
        padding: 10px;
        margin: 10px;
        height: 20px;
        width: 500px;
        border: 1px solid #eaeaea;
        outline: none;
        width: 800px;
    }

    input:hover {
        border-color: #a0a0a0 #b9b9b9 #b9b9b9 #b9b9b9;
    }

    input:focus {
        border-color: #4d90fe;
    }

    input[type="submit"] {
        border-radius: 2px;
        background: #f2f2f2;
        border: 1px solid #f2f2f2;
        color: #757575;
        cursor: default;
        font-size: 14px;
        font-weight: bold;
        width: 100px;
        padding: 0 16px;
        height: 36px;
    }

    input[type="submit"]:hover {
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        background: #f8f8f8;
        border: 1px solid #c6c6c6;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        color: #222;
    }

    .outer {
        width: 50%;
        height: 50%;
        overflow: auto;
        margin: auto;
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
    }

</style>

<div class="outer">
    <div class="main" style="width:800px; margin:0 auto;text-align:center;">

        <form action="{{route('search')}}" method="POST">
            {{csrf_field()}}
            <label>Write a word to get a snippet of information about it from Wikipedia</label>
            <input name="description" style="text-align:center" type="text"/>
            <input style="margin:auto;" type="submit" value="Go">
        </form>
        @if ($errors->any())
            <div class="alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>



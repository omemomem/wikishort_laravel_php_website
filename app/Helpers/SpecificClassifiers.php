<?php namespace App\Helpers;

use App\Helpers\Utilities as Utilities;
use phpDocumentor\Reflection\Types\Boolean;

Class SpecificClassifiers{

    /*
     * SpecificClassifiers class crops the html body of a given definition to a smaller and summarized definition
     */

    // Markers: is pos within a <b>, is pos at the start of a <p> , is pos before 'were','were','are','is',..., find 'the' before the pos.
    private const START_OF_SEARCH_WIKI = "From Wikipedia, the free encyclopedia";
    private const START_OF_SEARCH_HTML = "<div id=\"mw-content-text\" class=\"mw-body-content mw-content-ltr\" lang=\"en\"";
    private const WEIGHT_BOLD = 0.5;
    private const WEIGHT_BEFORE_HELPING_VERB = 0.2;
    private const WEIGHT_INSIDE_PARAGRAPH = 1.5;
    private const WEIGHT_DEF_NAME = 0.5;
    private static $NEURON_BOLD;
    private static $NEURON_DEF_NAME;
    private static $NEURON_BEFORE_HELPING_VERB;
    private static $NEURON_INSIDE_PARAGRAPH;


    // returns an array of 2 elements, the start and end pos in $body for the specific definition, first pos == start , second pos == end
    public static function getSpecificDef($body , $subject){
        $startOfSnippet = -1;
        $lengthOfSnippet = -1;
        // cut excess text in $body (not needed text)
        $start = strpos($body,self::START_OF_SEARCH_HTML);
        if($start === false){
            $start = strpos($body,self::START_OF_SEARCH_WIKI);
            if ($start === false){
                return null;
            }
        }
        $body = substr($body,$start);
        if($body === false){
            return null;
        }

        // generate all the cut points possible for the specific def to be cropped at
        $subjectArr = Utilities::generateInitials($subject);
        $initialPosArr = Utilities::getArrOfPosFromArrOfText($body, $subjectArr);

        // get the start position of the specific definition, the highest weight is the chosen pos to start
        $maxWeight = -1;
        $maxPos = -1;
        for ($i = 0; $i < count($subjectArr); $i++){
            // for each item get check all the occurrences of the word and check each one
            $as = $i;
            $allOccurrences = Utilities::getAllOccurencesArr($body,$subjectArr[$i]);
            $boldNodeArray = self::getArrOfNodeTags($body,'<b>','</b>'); // $boldNodeArray is an array of the NodeArray(0) and Length(1)
            $paraNodeArray = self::getArrOfNodeTags($body,'<p>','</p>'); // $paraNodeArray is an array of the NodeArray(0) and Length(1)
            foreach ($allOccurrences as $current){
                $currentWeight = self::getWeightOfWord($body, $current , $subject, $boldNodeArray, $paraNodeArray);
                if($currentWeight > $maxWeight){
                    $maxWeight = $currentWeight;
                    $maxPos = $current;
                }
            }
        }
        $startOfSnippet = $maxPos;
        // get the end pos of a definition end of paragraph
        $endOfSnippet = 0;
        for ($i = $startOfSnippet; $i<strlen($body)-3;$i++){
            if($body[$i] == '<' && $body[$i+1] == '/' && $body[$i+2] == 'p' && $body[$i+3] == '>'){
                $endOfSnippet = $i-1;
                break;
            }
        }
        // substr inputs a starting point and a length so the end point minus the start point produces the length we need
        $lengthOfSnippet = ($endOfSnippet - $startOfSnippet);
        return substr($body,$startOfSnippet,$lengthOfSnippet);
    }

    // returns a weight value of a given word, bold and para node arrays are the mapping arrays for the body according to the tags
    private static function getWeightOfWord($body, $wordPos, $def, $boldNodeArray, $paraNodeArray){

        self::$NEURON_BOLD = 0;
        self::$NEURON_BEFORE_HELPING_VERB = 0;
        self::$NEURON_INSIDE_PARAGRAPH = 0;
        self::$NEURON_DEF_NAME = 0;

        if(self::isWordInsideParagraph($wordPos,$paraNodeArray) === true){
            self::$NEURON_INSIDE_PARAGRAPH = 1;
        }else{
            // if a pos of word is not inside a paragraph then it is not a right pos
            return 0;
        }
        // CHECK IF POS IS BOLD. AT START OF <P>.
        if ( self::isWordBold($wordPos,$boldNodeArray) === true){
            self::$NEURON_BOLD = 1;
        }
        if ( self::isWordBeforeHelping() === true){
            self::$NEURON_BEFORE_HELPING_VERB = 1;
        }
        if(self::isWordSameAsDefName($body,$wordPos,$def) == true){
            self::$NEURON_DEF_NAME = 1;
        }

        return (self::$NEURON_BOLD * self::WEIGHT_BOLD) + (self::$NEURON_BEFORE_HELPING_VERB * self::WEIGHT_BEFORE_HELPING_VERB)
            + (self::$NEURON_INSIDE_PARAGRAPH * self::WEIGHT_INSIDE_PARAGRAPH) +  (self::$NEURON_DEF_NAME * self::WEIGHT_DEF_NAME);
    }

    // checks if position of word is inside a bold tag <b></b>
    private static function isWordBold($wordPos, $boldNodeArray){

        $paraLength = $boldNodeArray[1];
        $paraArr = $boldNodeArray[0];
        $leftBorder = 0;
        $rightBorder = $paraLength;

        // binary search on the node array
        while($leftBorder + 1 < $rightBorder){
            $mid = intval(($rightBorder + $leftBorder) / 2);
            $midNode = $paraArr[$mid];
            if ($midNode->isWithin($wordPos) == true){
                return true;
            }elseif($wordPos > $midNode->getEndPos()){
                $leftBorder = $mid;
            }elseif($wordPos < $midNode->getStartPos()){
                $rightBorder = $mid;
            }
        }

        return false;
    }

    // checks if pos of word is before any helping verb such as were,are,there, the, etc...
    private static function isWordBeforeHelping(){ return false;}

    // checks if position of word is inside a paragraph tag <p></p>
    private static function isWordInsideParagraph($wordPos , $paragraphNodeArray){

        $paraLength = $paragraphNodeArray[1];
        $paraArr = $paragraphNodeArray[0];
        $leftBorder = 0;
        $rightBorder = $paraLength;

        // binary search on the node array
        while($leftBorder + 1 < $rightBorder){
            $mid = intval(($rightBorder + $leftBorder) / 2);
            $midNode = $paraArr[$mid];
            if ($midNode->isWithin($wordPos) == true){
                return true;
            }elseif($wordPos > $midNode->getEndPos()){
                $leftBorder = $mid;
            }elseif($wordPos < $midNode->getStartPos()){
                $rightBorder = $mid;
            }
        }

        return false;
    }

    // checks if pos of word is equal to the given definition name
    private static function isWordSameAsDefName($body,$wordPos,$def){

        $startOfWord = -1;
        for($i = $wordPos; $i > 0; $i--){
                if($body[$i] == '>' || ctype_space($body[$i])){
                    $startOfWord = $i+1;
                }
        }
        for($j = 0;$j < strlen($def) ; $j++){
            if($body[$startOfWord + $j] != $def[$j]){
                return false;
            }
        }
        return true;
    }


    // $startTag,$endTag are the html tags (strings) to be looked for inside $body, put each pair of positions inside an array and return it.
    private static function getArrOfNodeTags(string $body, string $startTag,string $endTag){

        $nodeArray = array();
        $hasStarted = false;
        $bodyLength = strlen($body);
        $tagsLength = max(strlen($startTag), strlen($endTag));
        $currentStartPos = -1;
        $currentEndPos = -1;
        $count = 0;

        for($i = 0; $i < $bodyLength- $tagsLength ; $i++){
            // first check if $startTag is found otherwise check for $endTag
            if($hasStarted == false){
                $startFlag = true;
                // check if the $startTag is in the $i position, if it is then put $hasStarted flag turns true
                for ($j = 0; $j <strlen($startTag); $j++){
                    if ($body[($i + $j)] != $startTag[$j]){
                        $startFlag = false;
                    }
                }
                if($startFlag == false){
                    continue;
                }else{
                    $hasStarted = true;
                    $currentStartPos = $i;
                }
            }else if ($hasStarted == true){
                $endFlag = true;
                // check if the $endTag is in the $i position, if it is then put it in a node and inside the array
                for ($k = 0; $k < strlen($endTag); $k++){
                    if ($body[($i + $k)] != $endTag[$k]){
                        $endFlag = false;
                    }
                }
                if($endFlag == false){
                    continue;
                }else{
                    $hasStarted = false;
                    $currentEndPos = $i;

                    $tempNode = new Node($currentStartPos,$currentEndPos);
                    array_push($nodeArray,$tempNode);
                    $count++;
                }
            }
        }

        return array($nodeArray,$count);
    }





}

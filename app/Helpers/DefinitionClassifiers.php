<?php  namespace App\Helpers;


Class DefinitionClassifiers{

    /*
        DefinitionClassifiers determines whether a definition is disambiguate or specific using a neuron-like data structure.
    */

    private const MAY_REFER = "may refer to";
    private const DISAMBIGUOUS = " disambiguation ";
    private const ASSOC_WITH_TERM = "associated with the title term";
    private const WEIGHT_MAY_REFER = 0.35;
    private const WEIGHT_DISAMBIGUOUS = 0.8;
    private const WEIGHT_ASSOC_WITH_TERM = 0.35;
    private const OUTPUT_LIMIT = 0.4;
    private static $NEURON_MAY_REFER;
    private static $NEURON_DISAMBIGUOUS;
    private static $NEURON_ASSOC_WITH_TERM;


    // return is a term is indeed a general term (disambiguate term)
    public static  function isGeneralDef($body){

        self::$NEURON_MAY_REFER = 0;
        self::$NEURON_DISAMBIGUOUS = 0;
        self::$NEURON_ASSOC_WITH_TERM = 0;

        // fire up a neuron if the word is present in body
        if(DefinitionClassifiers::isPresent(self::MAY_REFER,$body)){
            self::$NEURON_MAY_REFER = 1;
        }

        if(DefinitionClassifiers::isPresent(self::DISAMBIGUOUS,$body)){
            self::$NEURON_DISAMBIGUOUS = 1;
        }

        if(DefinitionClassifiers::isPresent(self::ASSOC_WITH_TERM,$body)){
            self::$NEURON_ASSOC_WITH_TERM = 1;
        }

        // calculate the weight*neuron for each signal
        $calculatedNeurons = ((self::$NEURON_MAY_REFER * self::WEIGHT_MAY_REFER) + (self::$NEURON_DISAMBIGUOUS * self::WEIGHT_DISAMBIGUOUS) + (self::$NEURON_ASSOC_WITH_TERM * self::WEIGHT_ASSOC_WITH_TERM));

        if ($calculatedNeurons <= self::OUTPUT_LIMIT){
            return FALSE;
        }
        return TRUE;
    }

    // return is a given word is present in a given body
    private static function isPresent($word,$body){
        $generalTermPos = strpos(strtolower($body), strtolower($word));
        if ($generalTermPos === FALSE){
            return FALSE;
        }
        return TRUE;
    }

}

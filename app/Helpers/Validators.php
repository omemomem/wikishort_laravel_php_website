<?php namespace App\Helpers;

class Validators{

    // validate if given description is not empty and contain english letters, numbers and spaces only.
    public static function validateDescription($description)
    {
        if (empty($description))
            return false;
        if (ctype_alnum(str_replace(' ', '', $description)) === false) {
            return false;
        }
        return true;
    }



}

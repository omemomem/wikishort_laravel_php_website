<?php   namespace App\Helpers;


class Utilities{

    // convert a text to a wikipedia URL format.
    public static function textToWikiFormat($text)
    {
        $formatted = trim($text);
        str_replace(' ', '_', $formatted);
        return $formatted;
    }

    //
    public static function getArrOfPosFromArrOfText($body ,$textArr){

        $posArr = array();

        foreach ($textArr as $currentItem){
            $cutPos = strpos(strtolower($body), strtolower($currentItem));
            if ($cutPos >= 0) {
                array_push($posArr, $cutPos);
            }
        }
        return $posArr;
    }

    // generate an array of initials with a given text.
    public static function generateInitials($text){

        $text = trim($text);

        if (strlen($text) == 0){
            return null;
        }

        $initials = $text[0];
        // chars to insert after each letter for the initials.
        $arrOfChars = array('','.','/');
        // final array to be returned of all initials.
        $arrayOfInitials = array();


        // add all the intial letters in a given text to the string $intials.
        for ($i = 1; $i < strlen($text); $i++){
           if($text[$i] != ' ' && $text[$i-1] == ' '){
               $initials .= $text[$i];
           }
        }


        // if only one letter (one word was given) return the word only
        if(strlen($initials) <= 1){
            return array($text);
        }

        // make each initial from the array of chars and the clean initials string
        foreach ($arrOfChars as $ch){
            $arrayOfInitials[] = self::insertChar($initials,$ch);
        }


        return $arrayOfInitials;
    }


    // insert $char after each letter of $text
    private static function insertChar(&$text,&$char){

        $tempIntialsArr = str_split($text);
        $initials = '';
        for ($i = 0; $i < strlen($text)-1; $i++){
            $initials .= $tempIntialsArr[$i]. $char;
        }
        return $initials;
    }

    // get an array of positions for all the occurences of a given text
    public static function getAllOccurencesArr($body, $word){

        $body = strtolower($body);
        $word = strtolower($word);
        $positions = array();
        $bodyLen = strlen($body);
        $wordLen = strlen($word);
        $continueCount = 0;

        for ($i = 0; $i < $bodyLen - $wordLen ; $i++){

            // if we're inside a range of which the word was searched for then skip
            if($continueCount > 0){
                $continueCount--;
                continue;
            }

            $isTheWord = true;
            for ($j = 0; $j< $wordLen; $j++){
                if ($body[($i + $j)] != $word[$j]){
                    $isTheWord = false;
                    $continueCount = $wordLen;
                }
            }
            if($isTheWord == false){
                continue;
            }else{
                $positions[] = $i;
                $isTheWord = false;
                $continueCount = $wordLen;
            }
        }

        return $positions;
    }

}

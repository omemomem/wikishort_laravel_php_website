<?php

namespace App\Helpers;


class Node
{
    protected $startPos;
    protected $endPos;

    public function __construct(int $startPos, int $endPos ){
        $this->startPos = $startPos;
        $this->endPos = $endPos;
    }

    public function isWithin(int $pos){
        return ($this->startPos <= $pos && $pos <= $this->endPos );
    }

    public function getStartPos(){
        return $this->startPos;
    }

    public function getEndPos(){
        return $this->endPos;
    }


}

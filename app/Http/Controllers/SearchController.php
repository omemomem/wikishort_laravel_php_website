<?php

namespace App\Http\Controllers;
use App\Helpers\SpecificClassifiers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Helpers\Validators as Validators;
use App\Helpers\Utilities as Utilities;
use App\Helpers\DefinitionClassifiers as DefClassifiers;

class SearchController extends Controller
{
    protected const WIKI_URL = 'https://en.wikipedia.org/wiki/';

    // search and return a snippet from a Request.
    public function search(Request $request)
    {
        $description = trim($request->description);
        $url = self::WIKI_URL . Utilities::textToWikiFormat($description);
        $response = null;
        // $request->description
        if(!(Validators::validateDescription($description)))
            // TODO: return the lobby view WITH a validation error.
            return 'Validation failed';
        $response = Http::get($url);
        if ($response->failed())
            // TODO: return the lobby view WITH a response failed error.
            return "Your search came out empty! No information about it in wikipedia.";
        return self::generateSnippet($response->body(), $description);
    }
    // search and return a snippet from a text string (search function overloading).
    public function searchDirect($text){
        $tempRequest = new Request();
        $tempRequest->description = $text;
        return $this->search($tempRequest);
    }


    // generate the snippet/list of definitions, decide if the $body is a term or a list of definitions.
    private function generateSnippet($body, $subject)
    {
        if (DefClassifiers::isGeneralDef($body) === FALSE)
            return self::specificDef($body, $subject);
        return self::generalDef($body);
    }


    // format the $body as a general def, the returned object is not a snippet but a list of terms to choose from.
    private function generalDef($body)
    {
        $cutSignal = "retrieved from";
        $cutPos = strpos(strtolower($body), strtolower($cutSignal));

        // TODO: format the body where each <a href> leads to a search with the algorithm
        if ($cutPos == !FALSE) {
            $body = substr($body, 0, $cutPos);
            return ($body);
        }
        return $body;
    }


    // format the $body to a snippet.
    private function specificDef($body, $subject)
    {
        $specificDef = SpecificClassifiers::getSpecificDef($body , $subject);

        if($specificDef === null){
            return "SPECIFIC DEFINITION DOES NOT EXIST";
        }

        return $specificDef;
    }
}
